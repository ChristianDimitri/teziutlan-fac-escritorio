Imports System.Windows.Forms

Public Class DialogTiene_Facturas

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        FrmFAC.ContratoTextBox.Tag = "2"
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        If rYaGrabeFactura = 3 Then
            rYaGrabeFactura = 0
            Label1.Text = "El Cliente ya Tiene Pagado el Mes Actual"
        Else
            FrmFAC.ContratoTextBox.Tag = "1"
            Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Close()
        End If
    End Sub

    Private Sub DialogTiene_Facturas_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If rYaGrabeFactura = 2 Then
            rYaGrabeFactura = 0
            Label1.Text = "El Cliente ya Tiene Pagado el Mes Actual"
        End If
    End Sub
End Class
