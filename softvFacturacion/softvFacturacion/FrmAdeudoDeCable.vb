﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data

Public Class FrmAdeudoDeCable
  
  

    Private Sub FrmAdeudoDeCable_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        cobroCableV = 0
        metrajeCableV = 0
        CostoMetroCableV = 0
        metrajeabonadoCableV = 0
        saldoCableV = 0
        SaldoAbonadoCableV = 0
        cobroCableR = 0
        metrajeCableR = 0
        CostoMetroCableR = 0
        metrajeabonadoCableR = 0
        saldoCableR = 0
        SaldoAbonadoCableR = 0
        cobroCableAnterior = 0
        metrajeCableAnterior = 0
        CostoMetroCableAnterior = 0
        metrajeabonadoCableAnterior = 0
        saldoCableAnterior = 0
        SaldoAbonadoCableAnterior = 0
        Label4.Text = 0
        Label6.Text = 0
        Label12.Text = 0
        Label14.Text = 0
        Label10.Text = 0
        Label16.Text = 0
        Label23.Text = 0
        Label24.Text = 0
        Label28.Text = 0
        TextBox1.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox2.Text = ""
        TextBox2.Enabled = False
        TextBox4.Enabled = False
        TextBox6.Enabled = False
        TextBox3.Enabled = False
        TextBox1.Enabled = False
        TextBox5.Enabled = False
        If metrajeabonadoCableAnterior = 0 And metrajeabonadoCableR = 0 And metrajeabonadoCableV = 0 Then
            Me.Button1.Enabled = False
        End If
        Llenar_DataGridView()
    End Sub

   
    Private Sub Llenar_DataGridView()

        'Abro la conexion
        Using connection As New SqlConnection(MiConexion)
            Dim command As SqlCommand
            Dim adapter As SqlDataAdapter
            Dim dtTable As DataTable
            Dim command1 As SqlCommand
            Dim adapter1 As SqlDataAdapter
            Dim dtTable1 As DataTable

            'Indico el SP que voy a utilizar
            command = New SqlCommand("MuestraAdeudoMaterialCable", connection)
            command.CommandType = CommandType.StoredProcedure
            adapter = New SqlDataAdapter(command)
            dtTable = New DataTable
            With command.Parameters
                'Envio los parámetros que necesito
                .Add(New SqlParameter("@contrato", SqlDbType.Int)).Value = GloContrato
                .Add(New SqlParameter("@clv_session", SqlDbType.BigInt)).Value = gloClv_Session
            End With

            command1 = New SqlCommand("MuestraDescargaDeMaterialAntigua", connection)
            command1.CommandType = CommandType.StoredProcedure
            adapter1 = New SqlDataAdapter(command1)
            dtTable1 = New DataTable
            With command1.Parameters
                'Envio los parámetros que necesito
                .Add(New SqlParameter("@contrato", SqlDbType.Int)).Value = GloContrato
                .Add(New SqlParameter("@clv_session", SqlDbType.BigInt)).Value = gloClv_Session
            End With
            Try
                'Aqui ejecuto el SP y lo lleno en el DataTable
                adapter.Fill(dtTable)
                'Enlazo mis datos obtenidos en el DataTable con el grid
                DataGridView2.DataSource = dtTable
                'Si no pongo esta linea, se crean automáticamente los campos del grid dependiendo de los campos del DataTable
                DataGridView2.AutoGenerateColumns = False
                'Aqui le indico cuales campos del select de mi SP van con los campos de mi grid
                With DataGridView2
                    .Columns("Tipo_descarga").DataPropertyName = "Tipo_descarga"
                    .Columns("Arti").DataPropertyName = "Arti"
                    .Columns("Articulo").DataPropertyName = "Articulo"
                    .Columns("Descarga").DataPropertyName = "Descarga"
                    .Columns("Orden").DataPropertyName = "Orden"
                    .Columns("Bitacora").DataPropertyName = "Bitacora"
                    .Columns("MetrosTotales").DataPropertyName = "MetrosTotales"
                    .Columns("MetrosRestantes").DataPropertyName = "MetrosRestantes"
                    .Columns("AdeudoTotal").DataPropertyName = "AdeudoTotal"
                    .Columns("AdeudoRestante").DataPropertyName = "AdeudoRestante"
                    .Columns("Arti").Visible = False
                    .Columns("Descarga").Visible = False
                End With
                adapter1.Fill(dtTable1)
                'Enlazo mis datos obtenidos en el DataTable con el grid
                DataGridView1.DataSource = dtTable1
                'Si no pongo esta linea, se crean automáticamente los campos del grid dependiendo de los campos del DataTable
                DataGridView1.AutoGenerateColumns = False
                'Aqui le indico cuales campos del select de mi SP van con los campos de mi grid
                With DataGridView1
                    '.Columns("Tipo_descarga").DataPropertyName = "Tipo_descarga"
                    '.Columns("Articulo").DataPropertyName = "Articulo"
                    '.Columns("Orden").DataPropertyName = "Orden"
                    .Columns("Descarga").DataPropertyName = "Descarga"
                    '.Columns("Bitacora").DataPropertyName = "idBitacora"
                    '.Columns("Metraje_inicial").DataPropertyName = "Metraje_inicial"
                    '.Columns("Metraje_final").DataPropertyName = "Metraje_final"
                    .Columns("Cantidad_con_cobro").DataPropertyName = "Cantidad_con_cobro"
                    '.Columns("Cantidad_sin_cobro").DataPropertyName = "Cantidad_sin_cobro"
                    '.Columns("Metraje_inicial_exterior").DataPropertyName = "Metraje_inicial_exterior"
                    '.Columns("Metraje_fin_exterior").DataPropertyName = "Metraje_fin_exterior"
                    .Columns("Total_a_cobrar").DataPropertyName = "Total_a_cobrar"
                End With

               

            Catch expSQL As SqlException
                MsgBox(expSQL.ToString, MsgBoxStyle.OkOnly, "SQL Exception")
                Exit Sub
            End Try

        End Using


    End Sub

    'Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
    '    metrajeabonadoCableV = IsNumeric(TextBox1.Text)

    '    saldoCableV = (cobroCableV / metrajeabonadoCableV)
    '    TextBox2.Text = saldoCableV
    'End Sub

    Private Sub TextBox1_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.Enter
        metrajeabonadoCableV = IsNumeric(TextBox1.Text)
        If metrajeabonadoCableV > metrajeCableV Then
            MessageBox.Show("El numero de metros es mayor a los metros adeudados.")
            metrajeabonadoCableV = 0
            Exit Sub
        Else
            saldoCableV = (metrajeabonadoCableV * CostoMetroCableV)
            TextBox2.Text = saldoCableV
            If metrajeabonadoCableV > 0 Then
                Me.Button1.Enabled = True
            End If
        End If
     
    End Sub
    Private Sub txtId_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles TextBox1.KeyPress

        If AscW(e.KeyChar) = CInt(Keys.Enter) Then
            metrajeabonadoCableV = Convert.ToInt32(TextBox1.Text)
            If metrajeabonadoCableV > metrajeCableV Then
                MessageBox.Show("El numero de metros es mayor a los metros adeudados.")
                metrajeabonadoCableV = 0
                Me.TextBox1.Text = 0
                Exit Sub
            Else
                CostoMetroCableV = (cobroCableV / metrajeCableV)
                saldoCableV = (metrajeabonadoCableV * CostoMetroCableV)
                TextBox2.Text = saldoCableV
                SaldoAbonadoCableV = saldoCableV
                If metrajeabonadoCableV > 0 Then
                    Me.Button1.Enabled = True
                Else
                    Me.Button1.Enabled = False
                End If
            End If
            
        End If
    End Sub

    Private Sub DataGridView2_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellClick

        'metraje = CInt(DataGridView2.Rows(0).Cells("Cantidad_con_cobro").Value) + CInt(DataGridView2.Rows(1).Cells("Cantidad_con_cobro").Value)
        'cobro = CInt(DataGridView2.Rows(0).Cells("Total_a_cobrar").Value) + CInt(DataGridView2.Rows(1).Cells("Total_a_cobrar").Value)

        Dim totalR As Double = 0
        Dim total1R As Double = 0
        Dim totalV As Double = 0
        Dim total1V As Double = 0

        For Each fila As DataGridViewRow In DataGridView2.Rows
           If fila.Cells("Arti").Value = 1 Then
                CableV = Convert.ToDouble(fila.Cells("Arti").Value)
                DesdcargaCableV = Convert.ToDouble(fila.Cells("Descarga").Value)
            ElseIf fila.Cells("Arti").Value > 1 Then
                cableR = Convert.ToDouble(fila.Cells("Arti").Value)
                DesdcargaCableR = Convert.ToDouble(fila.Cells("Descarga").Value)
            End If
            If fila.Cells("Arti").Value Is Nothing Then

            End If

        Next




        For Each fila As DataGridViewRow In DataGridView2.Rows

            If fila.Cells("MetrosRestantes").Value Is Nothing Then

            Else
                If fila.Cells("Arti").Value = CableV Then
                    totalV += Convert.ToDouble(fila.Cells("MetrosRestantes").Value)
                    metrajeCableV = totalV
                End If
                If fila.Cells("Arti").Value = cableR Then
                    totalR += Convert.ToDouble(fila.Cells("MetrosRestantes").Value)
                    metrajeCableR = totalR
                End If
            End If

            If fila.Cells("AdeudoRestante").Value Is Nothing Then

            Else
                If fila.Cells("Arti").Value = CableV Then
                    total1V += Convert.ToDouble(fila.Cells("AdeudoRestante").Value)
                    cobroCableV = total1V
                End If
                If fila.Cells("Arti").Value = cableR Then

                    total1R += Convert.ToDouble(fila.Cells("AdeudoRestante").Value)
                    cobroCableR = total1R
                End If
            End If
        Next


        Label4.Text = metrajeCableV
        Label6.Text = cobroCableV
        Label10.Text = cobroCableV / metrajeCableV
        Label12.Text = metrajeCableR
        Label14.Text = cobroCableR
        Label16.Text = cobroCableR / metrajeCableR

        If metrajeCableV > 0 Then
            TextBox1.Enabled = True
        End If
        If metrajeCableR > 0 Then
            TextBox5.Enabled = True
        End If

        If cobroCableR = 0 Then
            Label16.Text = 0
        End If
        If cobroCableV = 0 Then
            Label10.Text = 0
        End If

    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Parcialidades = 1
        GuardaPreDetCobro(DesdcargaCableV, DesdcargaCableR, DesdcargaCableAnterior, gloClv_Session, SaldoAbonadoCableV, SaldoAbonadoCableR, SaldoAbonadoCableAnterior, SaldoAbonadoCableV + SaldoAbonadoCableR + SaldoAbonadoCableAnterior, metrajeabonadoCableV, metrajeabonadoCableR, metrajeabonadoCableAnterior, metrajeabonadoCableV + metrajeabonadoCableR + metrajeabonadoCableAnterior)
        Me.Close()
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        cobroCableV = 0
        metrajeCableV = 0
        CostoMetroCableV = 0
        metrajeabonadoCableV = 0
        saldoCableV = 0
        SaldoAbonadoCableV = 0
        cobroCableR = 0
        metrajeCableR = 0
        CostoMetroCableR = 0
        metrajeabonadoCableR = 0
        saldoCableR = 0
        SaldoAbonadoCableR = 0
        cobroCableAnterior = 0
        metrajeCableAnterior = 0
        CostoMetroCableAnterior = 0
        metrajeabonadoCableAnterior = 0
        saldoCableAnterior = 0
        SaldoAbonadoCableAnterior = 0
        Me.Close()
    End Sub
    Public Sub GuardaPreDetCobro(ByVal DESCARGA As Integer, ByVal DESCARGAR As Integer, ByVal DESCARGAA As Integer, ByVal SESSION As Integer, ByVal Montov As Integer, ByVal montor As Integer, ByVal montoA As Integer, ByVal IMPORTE As Integer, ByVal metrosv As Integer, ByVal metrosr As Integer, ByVal metrosA As Integer, ByVal METROS As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Descarga", SqlDbType.BigInt, DESCARGA)
        BaseII.CreateMyParameter("@DescargaR", SqlDbType.BigInt, DESCARGAR)
        BaseII.CreateMyParameter("@DescargaA", SqlDbType.BigInt, DESCARGAA)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, SESSION)
        BaseII.CreateMyParameter("@montov", SqlDbType.Money, Montov)
        BaseII.CreateMyParameter("@montor", SqlDbType.Money, montor)
        BaseII.CreateMyParameter("@montoA", SqlDbType.Money, montoA)
        BaseII.CreateMyParameter("@monto", SqlDbType.Money, IMPORTE)
        BaseII.CreateMyParameter("@metrosv", SqlDbType.Int, metrosv)
        BaseII.CreateMyParameter("@metrosr", SqlDbType.Int, metrosr)
        BaseII.CreateMyParameter("@metrosA", SqlDbType.Int, metrosA)
        BaseII.CreateMyParameter("@metros", SqlDbType.Int, METROS)

        BaseII.ProcedimientoOutPut("GuardaPreDetCobroParcialMaterial")
    End Sub
    Private Sub TextBox5_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox5.Enter

        metrajeabonadoCableR = IsNumeric(TextBox5.Text)
        If metrajeabonadoCableR > metrajeCableR Then
            MessageBox.Show("El numero de metros es mayor a los metros adeudados.")
            metrajeabonadoCableR = 0
            Exit Sub
        Else
            saldoCableR = (metrajeabonadoCableR * CostoMetroCableR)
            TextBox6.Text = saldoCableR
        End If
     
    End Sub
    Private Sub TextBox5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox5.KeyPress
        If AscW(e.KeyChar) = CInt(Keys.Enter) Then
            metrajeabonadoCableR = Convert.ToInt32(TextBox5.Text)
            If metrajeabonadoCableR > metrajeCableR Then
                MessageBox.Show("El numero de metros es mayor a los metros adeudados.")
                metrajeabonadoCableR = 0
                Me.TextBox5.Text = 0
                Exit Sub
            Else
                CostoMetroCableR = (cobroCableR / metrajeCableR)
                saldoCableR = (metrajeabonadoCableR * CostoMetroCableR)
                TextBox6.Text = saldoCableR
                SaldoAbonadoCableR = saldoCableR
                If metrajeabonadoCableR > 0 Then
                    Me.Button1.Enabled = True
                Else
                    Me.Button1.Enabled = False
                End If
            End If
           
        End If
    End Sub
    Private Sub TextBox3_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox3.Enter
        metrajeabonadoCableAnterior = IsNumeric(TextBox3.Text)
        If metrajeabonadoCableAnterior > metrajeCableAnterior Then
            MessageBox.Show("El numero de metros es mayor a los metros adeudados.")
            metrajeabonadoCableAnterior = 0
            Exit Sub
        Else
            saldoCableAnterior = (metrajeabonadoCableAnterior * CostoMetroCableAnterior)
            TextBox4.Text = saldoCableAnterior
        End If
       
    End Sub
    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If AscW(e.KeyChar) = CInt(Keys.Enter) Then
            metrajeabonadoCableAnterior = Convert.ToInt32(TextBox3.Text)
            If metrajeabonadoCableAnterior > metrajeCableAnterior Then
                MessageBox.Show("El numero de metros es mayor a los metros adeudados.")
                metrajeabonadoCableAnterior = 0
                Me.TextBox3.Text = 0
                Exit Sub
            Else
                CostoMetroCableAnterior = (cobroCableAnterior / metrajeCableAnterior)
                saldoCableAnterior = (metrajeabonadoCableAnterior * CostoMetroCableAnterior)
                TextBox4.Text = saldoCableAnterior
                SaldoAbonadoCableAnterior = saldoCableAnterior
                If metrajeabonadoCableAnterior > 0 Then
                    Me.Button1.Enabled = True
                Else
                    Me.Button1.Enabled = False
                End If
            End If
           
        End If
    End Sub
    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Dim totalR As Double = 0
        Dim total1R As Double = 0
        Dim totalV As Double = 0
        Dim total1V As Double = 0

        For Each fila As DataGridViewRow In DataGridView1.Rows

            If fila.Cells("Descarga").Value Is Nothing Then

            Else
                DesdcargaCableAnterior = Convert.ToDouble(fila.Cells("Descarga").Value)
            End If

        Next

        For Each fila As DataGridViewRow In DataGridView1.Rows

            If fila.Cells("MetrosRestantes").Value Is Nothing Then

            Else

                totalV += Convert.ToDouble(fila.Cells("MetrosRestantes").Value)
                metrajeCableAnterior = totalV

            End If

            If fila.Cells("AdeudoRestante").Value Is Nothing Then

            Else
                total1R += Convert.ToDouble(fila.Cells("AdeudoRestante").Value)
                cobroCableAnterior = total1R

            End If
        Next


        Label23.Text = metrajeCableAnterior
        Label24.Text = cobroCableAnterior
        Label28.Text = cobroCableAnterior / metrajeCableAnterior

        If metrajeCableAnterior > 0 Then
            TextBox3.Enabled = True
        End If

        If cobroCableAnterior = 0 Then
            Label28.Text = 0
        End If
    End Sub

   
End Class